﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new FileSystemLogger();
            logger.WriteData();
            logger = new DatabaseLogger();
            logger.WriteData();
            //New Data
            logger.WriteData();
            Console.ReadKey();
        }
    }
}
