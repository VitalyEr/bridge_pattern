﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    public class FileSystemLogger : Logger
    {
        public FileSystemLogger()
        {
            _loggerRep = new FileSystemRep();
        }
        public override void WriteData()
        {
            base.WriteData();
        }
    }
}
