﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    public class DatabaseLogger : Logger
    {
        public DatabaseLogger()
        {
            _loggerRep = new DatabaseRep();
        }
        public override void WriteData()
        {
            base.WriteData();
        }
    }
}
