﻿namespace Bridge
{
    //абстракция
    public abstract class Logger
    {
        protected LoggerRep _loggerRep;

        public virtual void WriteData()
        {
            _loggerRep.Write();
        }
    }
}